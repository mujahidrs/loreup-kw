import React from 'react';
import {
  Navbar,
  Nav,
  Button,
  Row,
  Col,
  Image,
  Card,
  Container
} from 'react-bootstrap';

function Header(){
  return (
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#home">
          <img
            alt=""
            src="https://loreup.id/assets/img/icon/logo.svg"
            width="100"
            height="100"
            className="d-inline-block align-top"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="#home">Beranda</Nav.Link>
            <Nav.Link href="#link">Daftar Kelas</Nav.Link>
            <Nav.Link href="#link">Diskon</Nav.Link>
            <Nav.Link href="#link">Kelas Gratis</Nav.Link>
            <Nav.Link href="#link">Masuk</Nav.Link>
            <Button variant="outline-primary">Daftar Gratis</Button>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
}

function Welcome(){
  return (
      <Row>
        <Col>
          <Image src="https://loreup.id/assets/img/hero-img.png" fluid />
        </Col>
        <Col>
          <Card>
            <Card.Body>
              <Card.Title>Temukan Passionmu dan Jadilah Seorang yang Ahli</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Cari minat bakatmu dan perdalam bersama kami. Mulai sekarang dan jelajahi apa yang kamu minati, dan siap-siap meraih masa depan yang cerah.</Card.Subtitle>
              <Button variant="primary">Mulai Jelajah</Button>
              <Card.Link href="#">Gabung Komunitas</Card.Link>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    );
}

function Feature(){
  const Fitur = (props) => {
    return(
        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src={props.image} />
          <Card.Body>
            <Card.Title>{props.title}</Card.Title>
            <Card.Text>
              {props.subtitle}
            </Card.Text>
          </Card.Body>
        </Card>
      );
  }

  return (
      <Row>
        <Col>
          <Fitur
            title="Kenali Passionmu"
            subtitle="Dengan belajar sesuai passionmu, semua akan terasa menyenangkan"
            image="https://loreup.id/assets/img/icon/target.svg"
          />
        </Col>
        <Col>
          <Fitur
            title="Gratis Materi Belajar"
            subtitle="Dapatkan akses gratis ke semua modul pembelajaran secara gratis!"
            image="https://loreup.id/assets/img/icon/gift.svg"
          />
        </Col>
        <Col>
          <Fitur
            title="Diskusi Forum Belajar"
            subtitle="Gabung grup telegram sebagai media bertanya dan sharing"
            image="https://loreup.id/assets/img/icon/message-square.svg"
          />
        </Col>
      </Row>
    );
}

function Program(){
  return (
      <div>
      </div>
    );
}

function Rank(){
  return (
      <div>
      </div>
    );
}

function Review(){
  return (
      <div>
      </div>
    );
}

function Footer(){
  return (
      <Row>
        <Col>
          <Row>
            <img
              alt=""
              src="https://loreup.id/assets/img/icon/logo.svg"
              width="100"
              height="100"
              className="d-inline-block align-top"
            />
          </Row>
          <Row>
            Lore Up adalah sebuah media e-learning untuk menunjang kebutuhan belajar online dan memperdalam minat dan bakat para anak muda
          </Row>
        </Col>
        <Col>
          Hak Cipta oleh Pandecoder 2020
        </Col>
      </Row>
    );
}

function App(){
  return(
      <Container>
        <Header/>
        <Welcome/>
        <Feature/>
        <Program/>
        <Rank/>
        <Review/>
        <Footer/>
      </Container>
    );
}

export default App;